<?php
$output = '';
 
/* Get the current resource's 'parent' field */
$parentId = $modx->resource->get('parent');
 
/* Get the parent object */
$parentObj = $modx->getObject('modResource', $parentId);
 
/* If we have the parent,
   get and return the pagetitle */
 
if ($parentObj) {
    $output = $parentObj->get('pagetitle');
}
 
return $output;