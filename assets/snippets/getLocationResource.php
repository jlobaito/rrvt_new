<?php
$output;
$location;
$resource;
$resoouceID;

/**
 * Gets the resource via ajax call to render correct location & related businesses.
 * TODO: sanitize the $_POST variable.
 * 
 */
if ( isset($_POST['location']) )
{
    $location = $_POST['location'];
    $resource = $modx->getObject('modResource', array('pagetitle' => $location, 'parent' => 72));

    if ( $resource )
    {
        $fields = $resource->toArray(); // An arrry of page fields (pagetitle, longtitle etc).

        $resoouceID = $fields['id'];
        $headline = $fields['pagetitle'];

        $output .= $modx->runsnippet('getResources', array(
            'parents' => $resoouceID,
            'tpl' => 'businessTeaserTpl',
            'showHidden' => 1,
            'default' => '<p>No business found for this location.</p>',
            'includeTVs' => 1,
            'processTVs' => 1,
            'tvPrefix' => '',
            'tvFilters' => 'featured==%yes%'
        ));
    }
    else
    {
        $output .= '<div class="alert alert-error">No information found for this location.</div>';
    }
}
else // No data to make getResources call.
{
    $output .= 'fail';
}

echo $output;