<?php
$vote = $_REQUEST['vote'];

//get content of textfile
$filename = "poll_result.txt";
$content = file($filename);

//put content in array
$array = explode("||", $content[0]);
$patricks = $array[0];
$uptown = $array[1];
$crossings = $array[2];
$pjs = $array[3];
$davids = $array[4];
$dexfield = $array[5];
$bellst = $array[6];

if ($vote == 0) {
	$patricks = $patricks + 1;
}
if ($vote == 1) {
	$uptown = $uptown + 1;
}

if ($vote == 2) {
	$crossings = $crossings + 1;
}

if ($vote == 3) {
	$pjs = $pjs + 1;
}

if ($vote == 4) {
	$davids = $davids + 1;
}

if ($vote == 5) {
	$dexfield = $dexfield + 1;
}

if ($vote == 6) {
	$bellst = $bellst + 1;
}

//insert votes to txt file
$insertvote = $patricks."||".$uptown."||".$crossings."||".$pjs."||".$davids."||".$dexfield."||".$bellst;
$fp = fopen($filename,"w");
fputs($fp,$insertvote);
fclose($fp);
?>
<div class="fade-in">
	<h2>Results:</h2>
	<div class="bars">
		<div class="patricks">
			<p>Patrick’s Restaurant, in Adel:</p>
			<div class="pieBar one" style="width:<?php echo(100*round($patricks/($uptown+$patricks+$crossings+$pjs+$davids+$dexfield+$bellst),7)); ?>%">
				<?php echo($patricks); ?>
			</div>
		</div>
		
		<div class="uptown">
				<p>Uptown Café, in Jefferson:</p>
			<div class="pieBar two" style="width:<?php echo(100*round($uptown/($uptown+$patricks+$crossings+$pjs+$davids+$dexfield+$bellst),7)); ?>%">
				<?php echo($uptown); ?>
			</div>
		</div>
		

		<div class="crossings">
				<p>Crossings Bistro &amp; Brew, in Minburn:</p>
			<div class="pieBar three" style="width:<?php echo(100*round($crossings/($uptown+$patricks+$crossings+$pjs+$davids+$dexfield+$bellst),7)); ?>%">
				<?php echo($crossings); ?>
			</div>
		</div>

		
		<div class="pjs">
			<p>PJ’s Drive-In, in Panora:</p>
			<div class="pieBar four" style="width:<?php echo(100*round($pjs/($uptown+$patricks+$crossings+$pjs+$davids+$dexfield+$bellst),7)); ?>%">
				<?php echo($pjs); ?>
			</div>
		</div>
		
		
		<div class="davids">
				<p>David’s Milwaukee Diner, Hotel Pattee, in Perry:</p>
			<div class="pieBar five" style="width:<?php echo(100*round($davids/($uptown+$patricks+$crossings+$pjs+$davids+$dexfield+$bellst),7)); ?>%">
				<?php echo($davids); ?>
			</div>
		</div>
		
		<div class="dexfield">
				<p>Dexfield Diner, in Redfield:</p>
			<div class="pieBar six" style="width:<?php echo(100*round($dexfield/($uptown+$patricks+$crossings+$pjs+$davids+$dexfield+$bellst),7)); ?>%">
				<?php echo($dexfield); ?>
			</div>
		</div>
		
		<div class="bellst">
				<p>Bell Street Market, in Yale:</p>
			<div class="pieBar seven" style="width:<?php echo(100*round($bellst/($uptown+$patricks+$crossings+$pjs+$davids+$dexfield+$bellst),7)); ?>%">
				<?php echo($bellst); ?>
			</div>
		</div>
	</div>
	<br>
	<p><strong>Total Votes: </strong><?php echo($uptown+$patricks+$crossings+$pjs+$davids+$dexfield+$bellst); ?></p>
</div>
