/**
 * Interactive map.
 * Uses Google Maps v3 API.
 * @param {funciton} $ | jQuery
 * @returns {undefined}
 *
 */
(function($) {
    var myLatlng1 = new google.maps.LatLng(53.65914, 0.072050);

    var RRVTMap = {
        'map': {},
        'mapOptions': {
            zoom: 1,
            mapTypeId: google.maps.MapTypeId.TERRAIN,
            scrollwheel: false,
            center: myLatlng1,
        },

        /**
         * Initialization method.
         * @returns {void}
         *
         */
        init: function() {
            var that = this;

            this.getUserLocation();

            $('#mapCanvas').html('<div class="map-loading"><p>Getting map locations, please wait.</p><p><span class="glyphicon glyphicon-refresh"></span></p></div>').hide().fadeTo('fast', 1, function() {
                that.getAvailableTowns(function(getAvailableTownsResponse) {
                    if (!$.isEmptyObject(getAvailableTownsResponse))
                    {
                        if ($('#mapCanvas .map-loading:visible'))
                        {
                            $('.map-loading').fadeTo('fast', 0, function() {
                                $(this).remove();
                                that.renderMap(getAvailableTownsResponse);
                            });
                        }
                    }
                    else // No data was returned.
                    {
                        if ($('#mapCanvas .map-loading:visible'))
                        {
                            $('.map-loading').fadeTo('fast', 0, function() {
                                $(this).addClass('alert alert-danger').html('Sorry, there was a problem getting map locatoins please try again later.').fadeTo('fast', 1, function() {
                                    // Done.
                                });
                            });
                        }
                    }
                });
            });
        },

        /**
         * Build the window caption after we have the user Geolocation
         * @param  {string}   name         Name of the location
         * @param  {object}   markerLatLng The Google LatLng object
         * @param  {function} callback     Callback
         * @return {void}
         */
        buildInfoWindowCaption: function( name, markerLatLng, callback ) {
            var html = '',
                trimmedName = '';

            // console.log(markerLatLng);

            trimmedName = name.substring(name.indexOf(','), 0);

            html = '<div class="marker-infowindow"><h2>' + trimmedName + '</h2>';
            if ( this.hasGeoLocation ) {

                this.getDistance({
                    userLat: this.userLat,
                    userLng: this.userLng,
                    destLat: markerLatLng.lat(),
                    destLng: markerLatLng.lng()
                }, function(distance) {
                    html += '<p>' + distance + '<p>';
                    html += '</div>';

                    callback(html);
                });
            } else {
                html += '</div>';
                callback(html);
            }
        },

        /**
         * Bunch up our locations into another array so we can calculate
         * the directions later on without hitting the 10 limit
         * @param  {array} locations Locations
         * @return {array}           Bunched locations
         */
        bunchUpLocations: function(locations) {
            var bunchedUpLocations = [[]],
                maxLocationsPerBunch = 5;

            $.each(locations, function(i, location) {
                var bunchIndex = Math.floor( i / maxLocationsPerBunch );

                if ( bunchedUpLocations[bunchIndex] ) {
                    bunchedUpLocations[bunchIndex].push(location);
                } else {
                    if ( bunchIndex !== 0 ) {
                        bunchedUpLocations[bunchIndex] = [ bunchedUpLocations[bunchIndex-1][ bunchedUpLocations[bunchIndex-1].length - 1]];
                        bunchedUpLocations[bunchIndex].push(location);
                    } else {
                        bunchedUpLocations[bunchIndex] = [location];
                    }
                }
            });

            return bunchedUpLocations;
        },
        /**
         * Gets a list of all available towns..
         * @param {object} callback | callback method.
         * @returns {object}
         *
         */
        getAvailableTowns: function(callback) {
            var availableTownsResponse = {};

            $.ajax({
                type: 'post',
                contentType: 'application/json',
                /*url: './locations_new.json',*/
                url: '/resources/data/locations.json',
                dataType: 'json',
                timeout: 50000
            }).done(function(availableTownsResponse) {
                callback(availableTownsResponse);
            }).fail(function(error) {
                callback(availableTownsResponse);
            });
        },

        /**
         * Get a distance between two points.
         * @param  {object} coords Contains the coordinates needed for the distance
         * @return {function}         Callback
         */
        getDistance: function( coords, callback ) {
            var distance = '';
            var userLat = coords.userLat,
                userLng = coords.userLng,
                destLat = coords.destLat,
                destLng = coords.destLng;

            var origin = new google.maps.LatLng(userLat, userLng);
            var destination = new google.maps.LatLng(destLat, destLng);
            var service = new google.maps.DistanceMatrixService();

            service.getDistanceMatrix({
                origins: [origin],
                destinations: [destination],
                travelMode: google.maps.TravelMode.BICYCLING,
                unitSystem: google.maps.UnitSystem.IMPERIAL
            }, function( response, status ) {

                // console.log(response);

                if ( status === 'OK' ) {
                    if ( response.rows[0].elements[0].status === 'ZERO_RESULTS' ) {
                        callback('<em>Distance not available</em>');
                    } else {
                        var distance = response.rows[0].elements[0].distance.text;

                        callback(distance + ' from you.');
                    }
                } else {
                    callback('<em>Distance not available</em>');
                }
            });
        },

        /**
         * Get the user's current location using the HTML5 API
         * @return {void}
         */
        getUserLocation: function() {
            if ( navigator.geolocation ) {
                navigator.geolocation.getCurrentPosition(function( position ) {
                    this.userLat = position.coords.latitude;
                    this.userLng = position.coords.longitude;

                    this.hasGeoLocation = true;
                }.bind(this), function(error) {
                    this.hasGeoLocation = false;
                }.bind(this));
            } else {
                this.hasGeoLocation = false;
            }
        },

        /**
         * Geocode any given physical address.
         *
         * NOW UNUSED
         *
         * @param {string} address | incoming address to geocode.
         * @param {object} callback | callback for latlng response back to function.
         * @returns {object}
         *
         */
        geoCodeAddress: function(address, callback) {
            var that = this;
            var geocoder = new google.maps.Geocoder();
            var results = [];
            var latlng = {};

            if (address !== '')
            {
                geocoder.geocode({'address': address}, function(results, status) {
                    switch (status)
                    {
                        case 'OK':
                            latlng.status = 'success';
                            latlng.name = address.substring(0, address.indexOf(','));
                            latlng.lat = results[0].geometry.location.lat();
                            latlng.lng = results[0].geometry.location.lng();

                            console.log({ name: latlng.name, lat: latlng.lat, lng: latlng.lng });
                            break;
                        case 'ZERO_RESULTS':
                            latlng.status = 'fail';
                            latlng.msg = 'ZERO_RESULTS';
                            break;
                        default:
                            latlng.status = 'fail';
                            latlng.msg = status;
                            break;
                    }

                    callback(latlng); // RRVTMap.init
                });
            }
            else // No address provided.
            {

            }
        },

        

        /**
         * Renders the map.
         * @param {object} locations | The locations to plot.
         * @returns {void}
         *
         */
        renderMap: function(locations) {
            var that = this;
            var bunchedUpLocations = [];
            var infowindow;
            var count = 0;
            var bounds;
            var idle;
            var marker = {};
            var markerLatLng = {};


           

            that.map = new google.maps.Map(document.getElementById('mapCanvas'), that.mapOptions);
            infowindow = new google.maps.InfoWindow();
            bounds = new google.maps.LatLngBounds();
            // Bunch up our locations to avoid rate limits
            bunchedUpLocations = this.bunchUpLocations(locations);
            // console.log(bunchedUpLocations);

            for (var i = 0; i < locations.length; i++)
            {
                var location = locations[i];

                count++;
                markerLatLng = {
                    lat: location.lat,
                    lng: location.lng
                };

                marker = new google.maps.Marker({
                    'map': that.map,
                    'clickable': true,
                    'position': markerLatLng,
                    'title': location.name,
                    'animation': google.maps.Animation.DROP
                });

                var myloc = new google.maps.Marker({
                    clickable: false,
                    icon: new google.maps.MarkerImage('//maps.gstatic.com/mapfiles/mobile/mobileimgs2.png',
                                                                    new google.maps.Size(22,22),
                                                                    new google.maps.Point(0,18),
                                                                    new google.maps.Point(11,11)),
                    zIndex: 999,
                    map: that.map,
                    animation: google.maps.Animation.DROP,
                });

                if (navigator.geolocation) navigator.geolocation.getCurrentPosition(function(pos) {
                    var me = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
                    myloc.setPosition(me);
                   
                });

                google.maps.event.addListener(marker, 'click', function(event) {
                    var clicky = this;
                    var locationName = this.title;

                    // Build the caption only on click
                    that.buildInfoWindowCaption(this.title, this.position, function(html) {
                        infowindow.close();
                        infowindow.setContent(html);
                        infowindow.open(that.map, clicky);

                        if ($('#mapLocations').css('display') === 'block')
                        {   
                            $('#mapLocationsNonFeatured').fadeTo('fast', 0, function() {
                                $(this).html('').remove();
                            });
                            $('#mapLocations').fadeTo('fast', 0, function() {
                                $(this).html('').remove();
                                that.doLocationSearch(locationName);
                            });
                        }
                        else
                        {
                            that.doLocationSearch(locationName);
                        }
                    });
                });

                bounds.extend(marker.position);

                if (count === locations.length)
                {
                    that.map.fitBounds(bounds);
                    
                    // Send our bunched up locations to the route calculator
                    that.calculateRoute(bunchedUpLocations);
                }
            }

            idle = google.maps.event.addListener(that.map, 'idle', function() {
                google.maps.event.removeListener(idle);
            });

        },

        doLocationSearch: function(location) {
            var locationName = location.substring( location.indexOf(','), 0);
            var that = this;
            $('#locationInfo').before('<div class="location-loading"><p>loading...</p></div>').hide().fadeTo('fast', 1, function() {
                $.ajax({
                    type: 'post',
                    url: '/locations',
                    data: {'location': locationName},
                    dataType: 'html',
                    timeout: 50000
                }).done(function(locationResponse) {
                    if (locationResponse !== 'fail')
                    {
                        
                        
                        
                        if ($('.location-loading').css('display') === 'block')
                        {
                            $('.location-loading').fadeTo('fast', 0, function() {
                                $(this).remove();

                                $('<div id="mapLocations"><h1>' + locationName + '</h1>' + locationResponse + '</div>').appendTo($('#locationInfo')).hide().fadeTo('fast', 1, function() {
                                    that.doAnotherLocationSearch(location); // non-featured
                                });
                            });
                        }
                    }
                    else // No location data.
                    {

                    }
                }).fail(function(error) { // Epic fail.
                });
            });
        },

        doAnotherLocationSearch: function(location) {     
            
            var locationName = location.substring( location.indexOf(','), 0);
            var that = this;
            $('#locationInfoOther').before('<div class="location-loading"><p>loading...</p></div>').hide().fadeTo('fast', 1, function() {
                $.ajax({
                    type: 'post',
                    url: '/locations-other',
                    data: {'location': locationName},
                    dataType: 'html',
                    timeout: 50000
                }).done(function(locationResponse) {
                    if (locationResponse !== 'fail')
                    {
                        if ($('.location-loading').css('display') === 'block')
                        {
                            $('.location-loading').fadeTo('fast', 0, function() {
                                $(this).remove();

                                $('<ul id="mapLocationsNonFeatured"><h3>Other locations in the area</h3>' + locationResponse + '</ul>').appendTo($('#locationInfoOther')).hide().fadeTo('fast', 1, function() {
                                    // Done.
                                });
                            });
                        }
                    }
                    else // No location data.
                    {

                    }
                }).fail(function(error) { // Epic fail.
                });
            });
        },

        /**
         * Calculate the routes (the lines on the map) using Google Direction service
         * Note that it stops at a max of 10 waypoints, and we have more.
         * That's why we need to send in the waypoints in bunches and do several calls.
         * @param  {array} batches An array of waypoints
         * @return {void}
         */
        calculateRoute: function(batches) {
            var that = this;
            var combinedResults;
            var unsortedResults = [{}]; // to hold the counter and the results themselves as they come back, to later sort
            var directionsResultsReturned = 0;
            var directionsService = new google.maps.DirectionsService();
            var directionsDisplay = new google.maps.DirectionsRenderer();
            directionsDisplay.setMap(that.map);
            directionsDisplay.setOptions({suppressMarkers: true});

            for (var k = 0; k < batches.length; k++) {
                var lastIndex = batches[k].length - 1;
                var start = batches[k][0].name;
                var end = batches[k][lastIndex].name;

                // trim first and last entry from array
                var waypts = [];
                waypts = batches[k];
                waypts.splice(0, 1);
                waypts.splice(waypts.length - 1, 1);

                var realwaypts = [];

                $.each(waypts, function(i, waypt) {
                    var location = {
                        location: waypt.name
                    };

                    realwaypts[i] = location;
                });

                var request = {
                    origin : start,
                    destination : end,
                    waypoints : realwaypts,
                    optimizeWaypoints: true,
                    travelMode : window.google.maps.TravelMode.BICYCLING
                };
                (function (kk) {
                    directionsService.route(request, function (result, status) {
                        if (status == window.google.maps.DirectionsStatus.OK) {

                            var unsortedResult = {
                                order : kk,
                                result : result
                            };
                            unsortedResults.push(unsortedResult);

                            directionsResultsReturned++;

                            if (directionsResultsReturned == batches.length) // we've received all the results. put to map
                            {
                                // sort the returned values into their correct order
                                unsortedResults.sort(function (a, b) {
                                    return parseFloat(a.order) - parseFloat(b.order);
                                });
                                var count = 0;
                                for (var key in unsortedResults) {
                                    if (unsortedResults[key].result != null) {
                                        if (unsortedResults.hasOwnProperty(key)) {
                                            if (count == 0) // first results. new up the combinedResults object
                                                combinedResults = unsortedResults[key].result;
                                            else {
                                                // only building up legs, overview_path, and bounds in my consolidated object. This is not a complete
                                                // directionResults object, but enough to draw a path on the map, which is all I need
                                                combinedResults.routes[0].legs = combinedResults.routes[0].legs.concat(unsortedResults[key].result.routes[0].legs);
                                                combinedResults.routes[0].overview_path = combinedResults.routes[0].overview_path.concat(unsortedResults[key].result.routes[0].overview_path);

                                                combinedResults.routes[0].bounds = combinedResults.routes[0].bounds.extend(unsortedResults[key].result.routes[0].bounds.getNorthEast());
                                                combinedResults.routes[0].bounds = combinedResults.routes[0].bounds.extend(unsortedResults[key].result.routes[0].bounds.getSouthWest());
                                            }
                                            count++;
                                        }
                                    }
                                }
                                directionsDisplay.setDirections(combinedResults);
                            }
                        }
                    });
                })(k);
            }
        }
    };


    RRVTMap.init();
})(jQuery);

